function tableData() {
  
  var request = new XMLHttpRequest();
  request.open("GET", "http://127.0.0.1:8000/api/students");
  request.send();
  request.onload = () => {
    if (request.status === 200) {
      var response_data = JSON.parse(request.response);
      var data = response_data.data.items;

      var col = [];
      for (var i = 0; i < data.length; i++) {
        for (var key in data[i]) {
          if (col.indexOf(key) === -1) {
            col.push(key);
          }
        }
      }
      var table = document.getElementById("table");
      for (var i = 0; i < data.length; i++) {
        var tr = table.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
          var tabCell = tr.insertCell(-1);
          tabCell.innerHTML = data[i][col[j]];
        }
      }
      var container = document.getElementById("showData");
      container.appendChild(table);
      
    } else {
      console.log(`error ${request.status}`);
    }
  };
}
function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  document.getElementById('Student').click(); 
  
  function formData(){
      const data = {
          name: document.querySelector('#name').value,
          age: document.querySelector('#age').value,
          gender: document.querySelector('#gender').value,
          address: document.querySelector('#address').value,
          dob: document.querySelector('#dob').value
      }
     var data_json = JSON.stringify(data);

     var request=new XMLHttpRequest();
      request.open("POST", "http://127.0.0.1:8000/api/students");
      request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8' )
      request.send(data_json);
  }
  window.onload = function(){
  var submit_ele = document.getElementById("submit");
  submit_ele.addEventListener("click", formData);
  }